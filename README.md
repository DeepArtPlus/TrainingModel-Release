![](https://i.imgur.com/dfSzZEX.jpg)
# Art+ 
## Our Website: https://taiwan-art.kchen.club/
You can read some stories of Taiwanese artists and artworks in this website. If you like the artist, try to do style transfer with the models provided below.
***

# Trained Models (Taiwan)
### https://gitlab.com/DeepArtPlus/TrainingModel-chainer-fast-neuralstyle
Feedforward style transfer models using [https://github.com/yusuketomoto/chainer-fast-neuralstyle]
***
# Trained Models (World)
### https://gitlab.com/DeepArtPlus/TrainingModel-fast-neural-style
Feedforward style transfer models using [https://github.com/jcjohnson/fast-neural-style] 


***
# Questionnaire
### https://goo.gl/S3EMkT    
Please help us to fill out the questionnaire. Your reply will be highly appreciated. Thank you.
***
# Slides of PyCon
### https://tw.pycon.org/2017/en-us/events/talk/348363376935567495/
Explanation of CNN and art style transfer
***
# Contact
If you have any problem, feel free to contact us. 
Email: peteryuhsin@gmail.com
